function s=diag_sum(A)
[m,n]=size(A);
a=min(m,n);
i=(a+1)/2;
A=A([1:a],[1:a]);
Z=flip(A);
if mod(a,2)==0
     s=sum(diag(A))+sum(diag(Z));
else
     s=sum(diag(A))+sum(diag(Z))-A(i,i);
end
end